package com.elnico;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApi {

	@GetMapping("/")
	public String index() {
		return MailBackend.composeMailMessage();
	}
}
