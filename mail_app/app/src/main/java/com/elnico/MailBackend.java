package com.elnico;

import java.util.*;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.nio.charset.Charset;

public class MailBackend {

	public static void initializeProducer() {
                new Thread(() -> {
                        // create instance for properties to access producer configs   
                        Properties props = new Properties();

                        //Assign localhost id
                        props.put("bootstrap.servers", "127.0.0.1:9092");
      
                        //Set acknowledgements for producer requests.      
                        props.put("acks", "all");
      
                        //If the request fails, the producer can automatically retry,
                        props.put("retries", 0);
      
                        //Specify buffer size in config
                        props.put("batch.size", 16384);
      
                        //Reduce the no of requests less than 0   
                        props.put("linger.ms", 1);
      
                        //The buffer.memory controls the total amount of memory available to the producer for buffering.   
                        props.put("buffer.memory", 33554432);
      
                        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
         
                        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

                        Producer<String, String> producer = new KafkaProducer<String, String>(props);
                        System.out.println("Mail producer initialized");

                        for (int i = 0; i < 10; i++) {
                                producer.send(new ProducerRecord<String, String>("sampler",  "inbox", composeMailMessage()));
                                System.out.println("Message sent successfully");

                                try {
                                        Thread.sleep(1000);
                                } catch (Exception e) {
                                }
                        }

                        producer.close();
                        MailDatabase.showMailLog();
                }).start();
        }

        public static void initializeConsumers(Runnable onReadyCallback) {
                // DispatchGroup is a synchroniztion mechanism that helps organize execution of the some code 
                // but only strongly after group of some async actions take place
                // This concept originated from the iOS development
                DispatchGroup dispatchGroup = new DispatchGroup();

                for (int i = 0; i < MailApp.NUMBER_OF_CONSUMERS; i++) {
                        new Thread(() -> {
                                dispatchGroup.enter();

                                Properties props = new Properties();
                                props.put("bootstrap.servers", "localhost:9092");
                                props.put("group.id", "test");
                                props.put("enable.auto.commit", "true");
                                props.put("auto.commit.interval.ms", "1000");
                                props.put("session.timeout.ms", "30000");
                                props.put("key.deserializer" , "org.apache.kafka.common.serialization.StringDeserializer");
                                props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

                                KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
      
                                //Kafka Consumer subscribes list of topics here.
                                consumer.subscribe(Arrays.asList("sampler"));
      
                                //print the topic name
                                System.out.println("Subscribed to topic " + "sampler");
      
                                System.out.println("Mail consumer initialized");
                                // We are done here, so we leave the dispatch group and let then this thread to dispatch the ongoing producer's messages on its own
                                dispatchGroup.leave();
      
                                recordHandling: {
                                while (true) {
                                        ConsumerRecords<String, String> records = consumer.poll(100);
                                        for (ConsumerRecord<String, String> record : records) {
                                                // print the offset,key and value for the consumer records.
                                                System.out.printf("offset = %d, key = %s, value = %s\n", record.offset(), record.key(), record.value()); 

                                                try {
                                                        ObjectMapper mapper = new ObjectMapper();

                                                        String value = record.value();
                                                        Email emailMessage = mapper.readValue(value, Email.class);

                                                        MailDatabase.writeMessageToLog(emailMessage.date, consumer.toString(), emailMessage.email, emailMessage.message);

                                                        consumer.unsubscribe();
                                                        consumer.close();
                                                        break recordHandling;
                                                } catch (JsonProcessingException e) {
                                                        e.printStackTrace();
                                                }
                                        }
                                }
                                }
                        }).start();
                }

                // All consumers are properly prepared, so its time to initialize the producer and start sending messages
                dispatchGroup.notify(onReadyCallback);
        }

	public static String composeMailMessage() {
                Random random = new Random();
                Date date = new Date();
                String email = randomEmail();
                String message = "Hola, tu código de acceso es " + (random.nextInt(9000) + 1000);

                Map<String, Object> map = new HashMap<>();
                map.put("date", date.toString());
                map.put("email", email);
                map.put("message", message);

                String json = "{}";
                try {
                        json = new ObjectMapper().writeValueAsString(map);
                } catch (JsonProcessingException e) {
                        e.printStackTrace();
                }
                return json + "\n";
        }

        private static String randomEmail() {
                final int emailLength = 3;

                int leftLimit = (int) '0';
                int rightLimit = (int) 'z';

                Random random = new Random();
                return random.ints(leftLimit, rightLimit + 1)
                        .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                        .limit(emailLength)
                        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                        .toString() + "@test.com";
        }
}
