package com.elnico;

import java.sql.*;
import java.util.*;

public class MailDatabase {

	private static Connection connection;

	public static void initializeLogsDatabase() {
                try {
                        connection = connectLogsDatabase();
                        System.out.println("Successfully connected to the logs MariaDB database\n");

                        showMailLog();
                } catch (SQLException e) {
                        e.printStackTrace();
                }
        }

	public static Connection connectLogsDatabase() throws SQLException {
                String connectionURL = "jdbc:mariadb://172.17.0.1:3306/maildb";

                Properties connectionProperties = new Properties();
                connectionProperties.setProperty("user", "nicolay_taran");
                connectionProperties.setProperty("password", "nrs2023");
                //props.setProperty("ssl", "true");

                return DriverManager.getConnection(connectionURL, connectionProperties);
        }

	public static void showMailLog() {
                if (connection != null) {
                        try {
                                String query = "SELECT date, customer, email, message FROM mail_logs";
                                PreparedStatement preparedStatement = connection.prepareStatement(query);

                                ResultSet resultSet = preparedStatement.executeQuery();
                                System.out.println("+++ MAIL LOG PREVIOUS MESSAGES +++\n");

                                while (resultSet.next()) {
                                        String date = resultSet.getString("DATE");
                                        String customer = resultSet.getString("CUSTOMER");
                                        String email = resultSet.getString("EMAIL");
                                        String message = resultSet.getString("MESSAGE");
                                        System.out.println(String.format("%s %s %s %s", date, customer, email, message));
                                }

                                System.out.println("\n+++ END OF PREVIOUS MESSAGES LOG +++");
                        } catch (SQLException e) {
                                e.printStackTrace();
                        }
                } else {
                        System.err.println("Unable to connect to mail log database");
                }
        }

	public static void writeMessageToLog(String date, String customer, String email, String message) {
                if (connection != null) {
                        try {
                                String query = "INSERT INTO mail_logs(date, customer, email, message) VALUES(?, ?, ?, ?)";
                                PreparedStatement preparedStatement = connection.prepareStatement(query);
                                preparedStatement.setString(1, date);
                                preparedStatement.setString(2, customer);
                                preparedStatement.setString(3, email);
                                preparedStatement.setString(4, message);

                                preparedStatement.executeUpdate();
                                System.out.println("Mail log updated successfully");
                        } catch (SQLException e) {
                                e.printStackTrace();
                        }
                } else {
                        System.err.println("Unable to connect to mail log database");
                }
        }
}
