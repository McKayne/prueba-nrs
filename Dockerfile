# Yep, always using Debian is soooooooo boring
FROM voidlinux/voidlinux:latest
USER root

# On build:

# Initial update
RUN xbps-install -Su -y || :
RUN xbps-install -u xbps -y
RUN xbps-install -Su -y

# Basic utils
RUN xbps-install git -y
RUN xbps-install wget -y
RUN xbps-install unzip -y

# Mail app source code
RUN git clone https://gitlab.com/McKayne/prueba-nrs.git

# Java
RUN wget https://services.gradle.org/distributions/gradle-7.6-bin.zip > download_log
RUN mkdir /opt/gradle; unzip -d /opt/gradle gradle-7.6-bin.zip; export PATH=$PATH:/opt/gradle/gradle-7.6/bin
RUN xbps-install openjdk17 -y

# Kafka
RUN wget https://dlcdn.apache.org/kafka/3.3.1/kafka_2.13-3.3.1.tgz
RUN tar -xzf kafka_2.13-3.3.1.tgz

# Changing shell
RUN xbps-install bash -y
RUN ln -sf /bin/bash /bin/sh

# On launch:
CMD /kafka_2.13-3.3.1/bin/zookeeper-server-start.sh /kafka_2.13-3.3.1/config/zookeeper.properties & sleep 5 && /kafka_2.13-3.3.1/bin/kafka-server-start.sh /kafka_2.13-3.3.1/config/server.properties & sleep 12 && cd /prueba-nrs/mail_app && export PATH=$PATH:/opt/gradle/gradle-7.6/bin && gradle clean && gradle build && gradle bootRun
