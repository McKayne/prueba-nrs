CREATE DATABASE IF NOT EXISTS `maildb`;
GRANT ALL ON `maildb`.* TO 'nicolay_taran'@'%';
USE `maildb`;
CREATE TABLE `mail_logs`(`id` INTEGER AUTO_INCREMENT, `date` TEXT, `customer` TEXT, `email` TEXT, `message` TEXT, PRIMARY KEY(`id`));
