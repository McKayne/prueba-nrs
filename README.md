# Prueba NRS



## Descripción

Mi tarea de prueba para NRS Group. Está usando Spring Boot y Apache Kafka plugins y está instalada en los containers de Docker separados

## Desploy

Para deploy este proyecto por favor executa esos comandos en su termilal: 

- git clone https://gitlab.com/McKayne/prueba-nrs.git && cd prueba-nrs
- docker build --no-cache -t mail_container .
- docker-compose up

## En resultado

Los mensajes de producer están guardados en MariaDB y van a presentarse después todos de log mensajes de producer acabaron y los consumidores ya están apagados

![](nrs-result.png)
